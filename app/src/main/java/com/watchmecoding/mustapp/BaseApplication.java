package com.watchmecoding.mustapp;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class BaseApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/SF-UI-Display-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
