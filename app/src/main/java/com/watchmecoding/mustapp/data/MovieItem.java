package com.watchmecoding.mustapp.data;


public class MovieItem {

    private String key;
    private String movieTitle;
    private String movieDescription;
    private String director;
    private String cast;
    private String release;
    private String Poster;
    private String imdb;
    private String trailer;
    private String watchNow;
    private String soundtrack;
    private String showtimeAvailable;
    private String titleInComment;

    public String getTitleInComment() {
        return titleInComment;
    }

    public void setTitleInComment(String titleInComment) {
        this.titleInComment = titleInComment;
    }


    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getWatchNow() {
        return watchNow;
    }

    public void setWatchNow(String watchNow) {
        this.watchNow = watchNow;
    }

    public String getSoundtrack() {
        return soundtrack;
    }

    public void setSoundtrack(String soundtrack) {
        this.soundtrack = soundtrack;
    }

    public String getShowtimeAvailable() {
        return showtimeAvailable;
    }

    public void setShowtimeAvailable(String showtimeAvailable) {
        this.showtimeAvailable = showtimeAvailable;
    }


    public String getImdb() {
        return imdb;
    }

    public void setImdb(String imdb) {
        this.imdb = imdb;
    }


    public String getPoster() {
        return Poster;
    }

    public void setPoster(String poster) {
        Poster = poster;
    }


    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getMovieDescription() {
        return movieDescription;
    }

    public void setMovieDescription(String movieDescription) {
        this.movieDescription = movieDescription;
    }


    @Override
    public String toString() {
        return this.getMovieTitle();
    }

}
