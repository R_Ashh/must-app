package com.watchmecoding.mustapp.data;

import java.util.ArrayList;


public class MovieDataSource {


    public static ArrayList<MovieItem> getList() {

        ArrayList<MovieItem> movies = new ArrayList<>();
        MovieItem fightClub = new MovieItem();
        fightClub.setKey("fc");
        fightClub.setTitleInComment("Fight Club");
        fightClub.setMovieTitle("Fight Club");
        fightClub.setRelease("1999");
        fightClub.setDirector("David Fincher");
        fightClub.setMovieDescription("A depressed man (Edward Norton) suffering from insomnia meets a strange soap salesman named Tyler Durden (Brad Pitt) and soon finds himself living in his squalid house after his perfect apartment is destroyed. The two bored men form an underground club with strict rules and fight other men who are fed up with their mundane lives. Their perfect partnership frays when Marla (Helena Bonham Carter), a fellow support group crasher, attracts Tyler's attention.");
        fightClub.setCast("Brad Pitt, Edward Norton, Helena Bonham Carter");
        fightClub.setPoster("http://preview.ibb.co/kuWRbR/fightclub.jpg");
        fightClub.setImdb("★8.8");
        fightClub.setTrailer("");
        fightClub.setWatchNow("");
        fightClub.setShowtimeAvailable("");
        fightClub.setSoundtrack("");
        movies.add(fightClub);

        MovieItem eternalSunshine = new MovieItem();
        eternalSunshine.setKey("es");
        eternalSunshine.setMovieTitle("Eternal Sunshine Spottless Mind");
        eternalSunshine.setTitleInComment("Eternal Sunshine Spottless Mind");
        eternalSunshine.setRelease("2004");
        eternalSunshine.setDirector("Michel Gondry");
        eternalSunshine.setMovieDescription("After a painful breakup, Clementine (Kate Winslet) undergoes a procedure to erase memories of her former boyfriend Joel (Jim Carrey) from her mind. When Joel discovers that Clementine is going to extremes to forget their relationship, he undergoes the same procedure and slowly begins to forget the woman that he loved. Directed by former music video director Michel Gondry, the visually arresting film explores the intricacy of relationships and the pain of loss.");
        eternalSunshine.setCast("Jim Carry, Kate Winselt");
        eternalSunshine.setPoster("https://image.ibb.co/kgd6bR/eternalsunshine.jpg");
        eternalSunshine.setImdb("★8.3");
        eternalSunshine.setTrailer("");
        eternalSunshine.setWatchNow("");
        eternalSunshine.setShowtimeAvailable("");
        eternalSunshine.setSoundtrack("");
        movies.add(eternalSunshine);

        MovieItem forrestGump = new MovieItem();
        forrestGump.setKey("fg");
        forrestGump.setMovieTitle("Forrest Gump");
        forrestGump.setTitleInComment("Forrest Gump");
        forrestGump.setRelease("1994");
        forrestGump.setDirector("Robert Zemeckis");
        forrestGump.setMovieDescription("Slow-witted Forrest Gump (Tom Hanks) has never thought of himself as disadvantaged, and thanks to his supportive mother (Sally Field), he leads anything but a restricted life. Whether dominating on the gridiron as a college football star, fighting in Vietnam or captaining a shrimp boat, Forrest inspires people with his childlike optimism. But one person Forrest cares about most may be the most difficult to save -- his childhood love, the sweet but troubled Jenny (Robin Wright).");
        forrestGump.setCast("Tom Hanks, Robin Wright");
        forrestGump.setImdb("★8.8");
        forrestGump.setPoster("https://image.ibb.co/fYxzwR/forrestgump.jpg");
        forrestGump.setTrailer("");
        forrestGump.setWatchNow("");
        forrestGump.setShowtimeAvailable("");
        forrestGump.setSoundtrack("");
        movies.add(forrestGump);

        MovieItem perksofbeingwallflower = new MovieItem();

        perksofbeingwallflower.setKey("pobwf");
        perksofbeingwallflower.setMovieTitle("Perks Of Being A Wallflower");
        perksofbeingwallflower.setTitleInComment("Perks Of Being A Wallflower");
        perksofbeingwallflower.setRelease("2012");
        perksofbeingwallflower.setDirector("Stephen Chbosky");
        perksofbeingwallflower.setMovieDescription("Socially awkward teen Charlie (Logan Lerman) is a wallflower, always watching life from the sidelines, until two charismatic students become his mentors. Free-spirited Sam (Emma Watson) and her stepbrother Patrick (Ezra Miller) help Charlie discover the joys of friendship, first love, music and more, while a teacher sparks Charlie's dreams of becoming a writer. However, as his new friends prepare to leave for college, Charlie's inner sadness threatens to shatter his newfound confidence.");
        perksofbeingwallflower.setCast("Logan Lerman, Emma Watson, Ezra Miller");
        perksofbeingwallflower.setImdb("★8.0");
        perksofbeingwallflower.setPoster("https://preview.ibb.co/grbX1R/perksofbeingawallflower.jpg");
        perksofbeingwallflower.setTrailer("");
        perksofbeingwallflower.setWatchNow("");
        perksofbeingwallflower.setShowtimeAvailable("");
        perksofbeingwallflower.setSoundtrack("");
        movies.add(perksofbeingwallflower);
        return movies;
    }
}
