package com.watchmecoding.mustapp.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.view.View;


import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.watchmecoding.mustapp.R;
import com.watchmecoding.mustapp.data.MovieItem;
import com.watchmecoding.mustapp.util.BlurImage;


import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MovieItemActivity extends AppCompatActivity implements View.OnClickListener {
    private MovieItem item;
    private int BLUR_PERCENTAGE = 70;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_item_activity);

        String json = getIntent().getAction();
        item = new Gson().fromJson(json, MovieItem.class);

        TextView imdbRate = findViewById(R.id.imdbNum);
        imdbRate.setText(item.getImdb());
        TextView director = findViewById(R.id.directorName);
        director.setText(item.getDirector());
        TextView cast = findViewById(R.id.castName);
        cast.setText(item.getCast());
        TextView overview = findViewById(R.id.overview);
        overview.setText(item.getMovieDescription());
        TextView release = findViewById(R.id.release);
        release.setText(item.getRelease());
        TextView movieTitle = findViewById(R.id.movieTitle);
        movieTitle.setText(item.getMovieTitle());
        TextView commentTitle = findViewById(R.id.titleComment);
        commentTitle.setText(item.getTitleInComment());
        setTitle("");
        blurImage();
        fetchData();


        ImageButton imgButton = findViewById(R.id.playTrailer);
        imgButton.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    private void blurImage(){

        final ImageView blurPoster = findViewById(R.id.blur);
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                blurPoster.setScaleType(ImageView.ScaleType.CENTER_CROP);
                blurPoster.setImageBitmap(BlurImage.fastblur(bitmap, 1f, BLUR_PERCENTAGE));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        blurPoster.setTag(target);

        Picasso.with(this).load(item.getPoster()).into(target);
    }
    private void fetchData() {
        final android.widget.ImageView tvPoster = findViewById(R.id.posterImage);
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                tvPoster.setScaleType(ImageView.ScaleType.FIT_CENTER);
//                setPallet(bitmap);
                tvPoster.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        tvPoster.setTag(target);

        Picasso.with(this).load(item.getPoster()).into(target);

    }

//    private void setBlur(int muted) {
//        Bitmap image = Bitmap.createBitmap(1000, 2000, Bitmap.Config.ARGB_8888);
//        image.eraseColor(muted);
//
//        Bitmap outputBitmap = Bitmap.createBitmap(image);
//        final RenderScript renderScript = RenderScript.create(this);
//        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
//        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);
//        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
//        theIntrinsic.setRadius(25f);
//        theIntrinsic.setInput(tmpIn);
//        theIntrinsic.forEach(tmpOut);
//        tmpOut.copyTo(outputBitmap);
//        ImageView imageView = findViewById(R.id.blur);
//        imageView.setImageBitmap(outputBitmap);
//    }



//    private void setPallet(Bitmap bitmap) {
//        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
//            @Override
//            public void onGenerated(Palette palette) {
//                int defaultValue = 0x000000;
//                int vibrant = palette.getVibrantColor(defaultValue);
//                int vibrantLight = palette.getLightVibrantColor(defaultValue);
//                int vibrantDark = palette.getDarkVibrantColor(defaultValue);
//                int muted = palette.getMutedColor(defaultValue);
//                int mutedLight = palette.getLightMutedColor(defaultValue);
//                int mutedDark = palette.getDarkMutedColor(defaultValue);
//
//                findViewById(R.id.pallet).setBackgroundColor(muted);
//                setBlur(muted);
//
//            }
//        });
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.playTrailer:
                playVideo(item);
                break;
            case R.id.home:
                    supportFinishAfterTransition();
                    break;
        }

    }

    private void playVideo(MovieItem item) {
        Intent intent = new Intent(this, VideoPlayerClass.class);
        intent.setAction(new Gson().toJson(item));
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        startActivity(intent);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
