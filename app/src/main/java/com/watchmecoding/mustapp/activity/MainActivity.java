package com.watchmecoding.mustapp.activity;


import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v4.util.Pair;
import android.view.View;



import com.google.gson.Gson;
import com.watchmecoding.mustapp.R;
import com.watchmecoding.mustapp.adapters.RecyclerAdapter;
import com.watchmecoding.mustapp.data.MovieDataSource;
import com.watchmecoding.mustapp.data.MovieItem;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class MainActivity extends AppCompatActivity {


    private static final String TAG = MainActivity.class.getSimpleName();


    public static final int EDITOR_ACTIVITY_REQUEST = 1001;
    private ArrayList<MovieItem> dataSource = MovieDataSource.getList();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("");
        fetchList();
        checkInternetStatement();

    }


    public void checkInternetStatement(){
        ConnectivityManager conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo.State mobile = conMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();
        NetworkInfo.State wifi = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();

        if (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) {
            Snackbar.make(findViewById(R.id.coordinator_layout), "Internet is Connected", Snackbar.LENGTH_INDEFINITE).show();

        } else if (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING) {
            Snackbar.make(findViewById(R.id.coordinator_layout), "Internet is not Available!", Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    public boolean isGPSEnabled (Context mContext){
        LocationManager locationManager = (LocationManager)
                mContext.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    public void openMovie(MovieItem movieItem, View view) {
        Intent intent = new Intent(this, MovieItemActivity.class);
        intent.setAction(new Gson().toJson(movieItem));
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);


        ActivityOptionsCompat options = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Pair first = Pair.create(view.findViewById(R.id.posterImage), "profile");
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, first);
            startActivity(intent, options.toBundle());

        }
    }


    private void fetchList() {
        RecyclerView view = findViewById(R.id.posterImage);
        view.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        view.setAdapter(new RecyclerAdapter(dataSource, this));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
