package com.watchmecoding.mustapp.adapters;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.watchmecoding.mustapp.R;
import com.watchmecoding.mustapp.activity.MainActivity;
import com.watchmecoding.mustapp.data.MovieItem;

import java.util.ArrayList;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private ArrayList<MovieItem> movieList;
    private MainActivity context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View layout;
        public ImageView image;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            image = v.findViewById(R.id.posterImage);
        }
    }

    public RecyclerAdapter(ArrayList<MovieItem> movieList, MainActivity mainActivity) {
        this.movieList = movieList;
        this.context = mainActivity;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapter.ViewHolder holder, int position) {
        final MovieItem movie = movieList.get(position);
        holder.itemView.setTransitionName("transition");
        Picasso picasso = new Picasso.Builder(context).loggingEnabled(true).listener(new Picasso.Listener() {
            @Override
            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                Toast.makeText(context, exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }).build();
        picasso.load(movie.getPoster()).into(holder.image);
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.openMovie(movie, holder.itemView);
            }
        });
        holder.layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                context.openMovie(movie, holder.itemView);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }
}
